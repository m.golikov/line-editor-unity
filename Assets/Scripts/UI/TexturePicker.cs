using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TexturePicker : MonoBehaviour
{
    public GridLayoutGroup grid;
    public ToggleGroup toggleGroup;
    public TextureToggle textureTogglePrefab;

    [HideInInspector] public UnityEvent<Texture> onTextureChanged = new UnityEvent<Texture>();

    private List<TextureToggle> _textureToggles = new List<TextureToggle>();
    
    public void Initialize(List<Texture> textures)
    {
        foreach (var texture in textures)
        {
            var textureToggle = Instantiate(textureTogglePrefab, grid.transform);
            textureToggle.image.texture = texture;
            textureToggle.toggle.group = toggleGroup;
            textureToggle.toggle.onValueChanged.AddListener(t =>
            {
                if (t)
                {
                    onTextureChanged.Invoke(texture);
                }
            });
            
            _textureToggles.Add(textureToggle);
        }
    }

    public void SetTexture(Texture texture)
    {
        foreach (var toggle in _textureToggles)
        {
            if (toggle.image.texture == texture)
            {
                toggle.toggle.isOn = true;
                return;
            }
        }

        _textureToggles[0].toggle.isOn = true;
    }
}
