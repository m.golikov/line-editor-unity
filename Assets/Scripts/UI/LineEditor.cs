using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineEditor : MonoBehaviour
{
    public abstract class Parameter
    {
        public abstract void Deactivate(LineEditor lineEditor);
    }
    
    [Serializable]
    public class ColorParameter : Parameter
    {
        public Image colorView;
        public Toggle colorToggle;

        public override void Deactivate(LineEditor lineEditor)
        {
            lineEditor.colorPicker.gameObject.SetActive(false);
            colorToggle.isOn = false;
        }
    }
    
    [Serializable]
    public class NumberParameter : Parameter
    {
        public InputField inputField;

        public override void Deactivate(LineEditor lineEditor)
        {
            
        }
    }

    [Serializable]
    public class TextureParameter : Parameter
    {
        public RawImage texture;
        public Toggle textureToggle;
        
        public override void Deactivate(LineEditor lineEditor)
        {
            lineEditor.texturePicker.gameObject.SetActive(false);
            textureToggle.isOn = false;
        }
    }
    
    public Line line;
    public UILineRenderer lineRenderer;
    public Image background;

    [Header("Line points settings")]
    public LinePointController linePointControllerPrefab;
    public RectTransform linePointsParent;
    public Button spawnPointButton, deletePointButton;
    
    [Header("Parameters settings")]
    public TextureParameter lineTexture;
    public ColorParameter lineColor, backgroundColor;
    public NumberParameter lineAlpha, lineSize;
    public List<Texture> textures;

    [Header("Pickers settings")]
    public FlexibleColorPicker colorPicker;
    public TexturePicker texturePicker;

    private Parameter _currentParameter;
    private List<LinePointController> _linePointControllers = new List<LinePointController>();

    public const int PointCount = 2;
    
    private void Start()
    {
        spawnPointButton.onClick.AddListener(() =>
        {
            if (line.points.Count == PointCount)
            {
                return;
            }
            
            var linePointController = Instantiate(linePointControllerPrefab, linePointsParent);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(linePointsParent,
                Input.mousePosition, null, out var localPosition);
            linePointController.transform.localPosition = localPosition;
            linePointController.bounds = linePointsParent;
            _linePointControllers.Add(linePointController);

            var point = new GameObject($"Point_{_linePointControllers.Count}")
            {
                transform =
                {
                    parent = line.transform
                }
            };

            linePointController.linePoint = point.transform;
            line.points.Add(point.transform);
        });
        
        deletePointButton.onClick.AddListener(() =>
        {
            foreach (var pointController in _linePointControllers)
            {
                Destroy(pointController.gameObject);
            }

            foreach (var point in line.points)
            {
                Destroy(point.gameObject);
            }
            
            _linePointControllers.Clear();
            line.points.Clear();
        });
        
        texturePicker.Initialize(textures);
        
        InitializeColorParameter(lineColor, c => lineRenderer.lineColor = c, lineRenderer.lineColor);
        InitializeColorParameter(backgroundColor, c => background.color = c, background.color);
        
        InitializeNumberParameter(lineAlpha, s =>
            {
                if (int.TryParse(s, out var res))
                {
                    lineRenderer.lineAlpha = res / 100f;
                }
            }, 
            ((int)(lineRenderer.lineAlpha * 100)).ToString());
        InitializeNumberParameter(lineSize, s =>
        {
            if (int.TryParse(s, out var res))
            {
                line.size = res;
            }
        }, line.size.ToString());
        
        texturePicker.gameObject.SetActive(true);
        
        InitializeTextureParameter(lineTexture, t => lineRenderer.lineTexture = t, 
            lineRenderer.lineTexture);
        
        texturePicker.gameObject.SetActive(false);
    }

    private void InitializeColorParameter(ColorParameter parameter, Action<Color> colorSetter, Color defaultColor)
    {
        void SetColor(Color color)
        {
            parameter.colorView.color = color;
            colorSetter(color);
        }
        
        SetColor(defaultColor);
        colorPicker.color = defaultColor;
        
        parameter.colorToggle.onValueChanged.AddListener(t =>
        {
            _currentParameter?.Deactivate(this);
            _currentParameter = t ? parameter : null;
            
            if (t)
            {
                colorPicker.gameObject.SetActive(true);
                colorPicker.onColorChange.AddListener(SetColor);
            }
            else
            {
                colorPicker.onColorChange.RemoveListener(SetColor);
            }
        });
    }
    
    private void InitializeNumberParameter(NumberParameter parameter, Action<string> numberSetter, string defaultNumber)
    {
        parameter.inputField.contentType = InputField.ContentType.IntegerNumber;
        parameter.inputField.text = defaultNumber;

        parameter.inputField.onValueChanged.AddListener(s =>
        {
            _currentParameter?.Deactivate(this);
            _currentParameter = parameter;

            numberSetter(s);
        });
    }
    
    private void InitializeTextureParameter(TextureParameter parameter, Action<Texture> textureSetter, Texture defaultTexture)
    {
        void SetLineTexture(Texture texture)
        {
            parameter.texture.texture = texture;
            textureSetter(texture);
        }
        
        SetLineTexture(defaultTexture);
        texturePicker.SetTexture(defaultTexture);
        
        parameter.textureToggle.onValueChanged.AddListener(t =>
        {
            _currentParameter?.Deactivate(this);
            _currentParameter = t ? parameter : null;

            if (t)
            {
                texturePicker.gameObject.SetActive(true);
                texturePicker.onTextureChanged.AddListener(SetLineTexture);
            }
            else
            {
                texturePicker.onTextureChanged.RemoveListener(SetLineTexture);
            }
        });
    }
}
