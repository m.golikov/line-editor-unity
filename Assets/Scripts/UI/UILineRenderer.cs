using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILineRenderer : MonoBehaviour
{
    public Line line;
    
    [Header("Render settings")]
    public ComputeShader lineShader;
    public RawImage image;

    [Header("Line view settings")]
    public Texture lineTexture;
    public Color lineColor;
    public float lineAlpha;

    private RenderTexture _renderTexture;

    private void Start()
    {
        _renderTexture = new RenderTexture(8, 8, 24)
        {
            enableRandomWrite = true
        };
        
        image.texture = _renderTexture;
    }

    private void Update()
    {
        StartCoroutine(RenderLine());
    }

    private IEnumerator RenderLine()
    {
        yield return new WaitForEndOfFrame();

        var size = image.rectTransform.GetRectTransformSize();
        
        var width = (int) size.x;
        var height = (int) size.y;

        if (width != _renderTexture.width || height != _renderTexture.height)
        {
            _renderTexture.Release();
            _renderTexture = new RenderTexture(width, height, 24)
            {
                enableRandomWrite = true
            };
            image.texture = _renderTexture;
        }
        
        var kernel = lineShader.FindKernel("RenderLine");
        
        lineShader.SetTexture(kernel, "result", _renderTexture);
        lineShader.SetInt(Shader.PropertyToID("render_width"), width);
        lineShader.SetInt(Shader.PropertyToID("render_height"), height);
        
        lineShader.SetTexture(kernel, "line_texture", lineTexture);
        lineShader.SetInt(Shader.PropertyToID("texture_width"), lineTexture.width);
        lineShader.SetInt(Shader.PropertyToID("texture_height"), lineTexture.height);
        lineShader.SetInt(Shader.PropertyToID("line_size"), line.size);
        lineShader.SetFloat(Shader.PropertyToID("line_alpha"), lineAlpha);
        lineShader.SetFloats(Shader.PropertyToID("line_color"), lineColor.r, lineColor.g, lineColor.b, 1);

        if (line.points.Count == 0)
        {
            lineShader.SetInts(Shader.PropertyToID("pos1"), 0, 0);
            lineShader.SetInts(Shader.PropertyToID("pos2"), 0, 0);
        }
        else
        {
            lineShader.SetInts(Shader.PropertyToID("pos1"), Utils.GetUIPosition(line.points[0]));
            lineShader.SetInts(Shader.PropertyToID("pos2"), Utils.GetUIPosition(line.points[line.points.Count > 1 ? 1 : 0]));
        }
        
        lineShader.Dispatch(kernel, _renderTexture.width, _renderTexture.height, 1);
    }
}
