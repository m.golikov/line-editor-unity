using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class GridHeightController : MonoBehaviour
{
    public GridLayoutGroup grid;

    private RectTransform _gridRectTransform;
    
    private void Start()
    {
        _gridRectTransform = grid.GetComponent<RectTransform>();
    }

    private void Update()
    {
        if (!grid || !_gridRectTransform) return;
        
        var rows = Mathf.CeilToInt(grid.transform.childCount * 1.0f / grid.constraintCount);
        _gridRectTransform.sizeDelta = new Vector2(_gridRectTransform.sizeDelta.x, 
            grid.padding.bottom + grid.padding.top + grid.spacing.y * (rows - 1) + grid.cellSize.y * rows);
    }
}
