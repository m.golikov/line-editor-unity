using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static Vector2 GetRectTransformSize(this RectTransform rect)
    {
        var v = new Vector3[4];
        rect.GetWorldCorners(v);

        var v1 = RectTransformUtility.WorldToScreenPoint(null, v[0]);
        var v2 = RectTransformUtility.WorldToScreenPoint(null, v[2]);

        return new Vector2(v2.x - v1.x, v2.y - v1.y);
    }

    public static int[] GetUIPosition(Transform point)
    {
        var position = point.position;
        return new[] { (int) position.x, (int) position.y };
    }
}
