using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class LinePointController : UIBehaviour, IDragHandler
{
    public Transform linePoint;
    public RectTransform bounds;

    protected override void Start()
    {
        base.Start();

        linePoint.position = GetLinePositionFromUI(RectTransformUtility.WorldToScreenPoint(null,
            transform.position - bounds.transform.position));
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        var screenPosition = RectTransformUtility.WorldToScreenPoint(null, 
            transform.position - bounds.transform.position);
        screenPosition += eventData.delta;

        var boundsSize = bounds.GetRectTransformSize();

        screenPosition = new Vector3(
            Mathf.Clamp(screenPosition.x, -boundsSize.x * 0.5f, boundsSize.x * 0.5f), 
            Mathf.Clamp(screenPosition.y, -boundsSize.y * 0.5f, boundsSize.y * 0.5f), 
            0);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(bounds, 
            screenPosition + RectTransformUtility.WorldToScreenPoint(null, bounds.transform.position), 
            null, out var localPosition);
        
        transform.localPosition = localPosition;
        linePoint.position = GetLinePositionFromUI(screenPosition);
    }

    public Vector3 GetUIPositionFromLine(Vector3 linePosition)
    {
        var boundsSize = bounds.GetRectTransformSize();
        
        return new Vector3(linePosition.x - boundsSize.x * 0.5f, linePosition.y - boundsSize.y * 0.5f, 0);
    }

    public Vector3 GetLinePositionFromUI(Vector3 uiPosition)
    {
        var boundsSize = bounds.GetRectTransformSize();
        
        return new Vector3(uiPosition.x + boundsSize.x * 0.5f, uiPosition.y + boundsSize.y * 0.5f, 0);
    }
}
